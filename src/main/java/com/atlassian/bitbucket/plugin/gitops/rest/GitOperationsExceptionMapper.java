package com.atlassian.bitbucket.plugin.gitops.rest;

import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapper;
import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class GitOperationsExceptionMapper extends UnhandledExceptionMapper {
    public GitOperationsExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }
}
