package com.atlassian.bitbucket.plugin.gitops.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.rest.RestResource;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(ResourcePatterns.REPOSITORY_URI)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
@AnonymousAllowed
public class GitOperationsResource extends RestResource {

    private static final Logger log = LoggerFactory.getLogger(GitOperationsResource.class);

    private final GitCommandBuilderFactory gitCommandBuilderFactory;
    private final PermissionValidationService permissionValidationService;

    public GitOperationsResource(GitCommandBuilderFactory gitCommandBuilderFactory, PermissionValidationService permissionValidationService, I18nService i18nService) {
        super(i18nService);
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
        this.permissionValidationService = permissionValidationService;
    }

    @POST
    @Path(value = "gc")
    public Response triggerGarbageCollection(@Context Repository repository) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        log.debug("Triggering garbage collection for repository {}/{}", repository.getProject().getKey(), repository.getSlug());
        gitCommandBuilderFactory.builder(repository).gc().build().call();
        log.info("Garbage collection finished for repository {}/{}", repository.getProject().getKey(), repository.getSlug());
        return ResponseFactory.noContent().build();
    }

}
