define('plugin/git-ops/repository-settings', [
    'jquery',
    'aui',
    'bitbucket/util/server',
    'bitbucket/util/navbuilder',
    'bitbucket/util/state',
    'exports'
], function (
    $,
    AJS,
    server,
    nav,
    pageState,
    exports
) {
    function resourceUrl(resourceName) {
        return nav.rest('git-operations')
            .addPathComponents('projects', pageState.getProject().key, 'repos', pageState.getRepository().slug, resourceName)
            .build();
    }

    function initGarbageCollectButton(gcButtonSelector) {
        var $button = $(gcButtonSelector),
            $spinner;

        function addSpinner() {
            $spinner = $("<div class='button-spinner'></div>").insertAfter($button);
        }

        function removeSpinner() {
            $spinner && $spinner.remove();
            $spinner = null;
        }

        function setDeleteButtonEnabled(enabled) {
            if (enabled) {
                $button.removeAttr("disabled").removeClass("disabled");
            } else {
                $button.attr("disabled", "disabled").addClass("disabled");
            }
        }

        $button.click(function () {
            addSpinner();
            setDeleteButtonEnabled(false);
            $spinner.spin();
            server.rest({
                url: resourceUrl('gc'),
                type: 'POST'
            }).always(function () {
                removeSpinner();
                setDeleteButtonEnabled(true)
            });
        });
    }

    exports.onReady = function () {
        initGarbageCollectButton("#trigger-gc-button");
    }
});
