<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.7</version>
    </parent>

    <groupId>com.atlassian.bitbucket.plugin</groupId>
    <artifactId>bitbucket-git-ops-plugin</artifactId>
    <version>2.0.2-SNAPSHOT</version>

    <organization>
        <name>Seb Ruiz</name>
        <url>http://www.atlassian.com/bitbucket</url>
    </organization>

    <name>Repository git operations plugin</name>
    <description>Provides an interface to execute a variety of git operations on a hosted Bitbucket Server repository</description>
    <packaging>atlassian-plugin</packaging>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/stash-git-ops-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/stash-git-ops-plugin.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.bitbucket.server</groupId>
                <artifactId>bitbucket-parent</artifactId>
                <version>${bitbucket.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-git-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-module</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-rest-model</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>bitbucket-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <products>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket</instanceId>
                            <version>${bitbucket.version}</version>
                            <dataVersion>${bitbucket.data.version}</dataVersion>
                        </product>
                    </products>
                    <systemPropertyVariables>
                        <!-- Our func tests don't know how to wait for the system to be available, so force
                             it to come up synchronously so that Cargo will block for us -->
                        <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                    </systemPropertyVariables>
                </configuration>
                <dependencies>
                    <!-- jaxb-core isn't a transitive dependency of jaxb-impl in newer versions. -->
                    <dependency>
                        <groupId>com.sun.xml.bind</groupId>
                        <artifactId>jaxb-impl</artifactId>
                        <version>2.2.11</version>
                    </dependency>
                    <dependency>
                        <groupId>com.sun.xml.bind</groupId>
                        <artifactId>jaxb-core</artifactId>
                        <version>2.2.11</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <bitbucket.version>4.0.0-rc12</bitbucket.version>
        <bitbucket.data.version>${bitbucket.version}</bitbucket.data.version>
        <amps.version>6.1.0</amps.version>
        <plugin.testrunner.version>1.1</plugin.testrunner.version>
    </properties>
</project>
